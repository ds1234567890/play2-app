package controllers

import model.SampleModel
import play.api.data.Form
import play.api.mvc.Action
import play.api.mvc._
import play.api.data.Forms._

/**
 */
object SampleController extends Controller {

  /** FormのMapping
    Mapping情報、構築(apply)、分解(unapply)情報を定義
    */
  val sampleModel = Form(
    mapping("id" -> text, "name" -> text, "num" -> text)(SampleModel.apply)(SampleModel.unapply)
  )

  /**
   * 初期表示
   */
  def index = Action {
    Ok(views.html.input())
  }

  /**
   * 入力
   */
  def input = Action { implicit request =>
    val sample : SampleModel = sampleModel.bindFromRequest.get
    println("input:" + sample)
    Ok(views.html.confirm(sample))
  }

  /**
   * 確定
   */
  def confirm = Action { implicit request =>
    val sample = sampleModel.bindFromRequest.get
    println("confirm:" + sample)
    Ok(views.html.result("success"))
  }
}
